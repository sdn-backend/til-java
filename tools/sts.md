## Spring Tool Suite

### 사용 기술
- gradle 6.6.x (이후 버전은 멀티 프로젝트 버전)

### 초기 세팅
> Preferences
- `JDK` 
    - Java>Installed JREs
- `complier`
    - Java>Complier>`11버전`
- `encoding`
    - General>Content Types>`UTF-8`
- `gradle`
    - Gradle>Automatic Project ... 체크
